#Tutorial on installing Meanjs and Yeoman
##and anything you may need to run this project
######Author: Giannis Skaris | email: skarisg@gmail.com | Date: 13/8/2015 | OS: Ubuntu 14.04.3 LTS


**There are some packets i installed and i am not sure if these packets helped so i will mention the commands below anyway**

> $sudo apt-get install --yes build-essential

> $sudo apt-get install python-software-properties


##**Phase 1| Installing Mongo Database**

Tutorial on installing and running mongodb on Ubuntu you can find here:[HERE](http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/)

If you don't like opening links, follow the instructions below.



Run the following commands on SSH Terminal:

  > $sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10

  > $echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list

  > $sudo apt-get update

  > $sudo apt-get install -y mongodb-org=3.0.4 mongodb-org-server=3.0.4 mongodb-org-shell=3.0.4 mongodb-org-mongos=3.0.4 mongodb-org-tools=3.0.4
  
Done! Your Mongo Database is already active. Read MongoDB Docs [HERE](http://docs.mongodb.org/manual/) for more information!


##**Phase 2| Installing Nodejs v0.12.7**
  Steps:
  
1. Download Nodejs from: [HERE](https://nodejs.org/)
2. Open SSH Terminal
3. Goto the place the downloaded file is. Untar it
4. Run the following commands on SSH Terminal:

  > $sudo apt-get install g++ curl libssl-dev apache2-utils
  
  > $cd node-v0.12.7/     **//ignore if you already are inside this folder**
  
  > $./configure
  
  > $sudo make
  
  > $sudo make install
  
  > $node -v

If the last command returns v0.12.7, then everything is fine!


##**Phase 3| Installing Npm**

After installing nodejs, we already have an older version of npm installed on our pc, but we need the latest!

There is a problem with this updating, cause if you just try to update the npm, you will get the 2.x.x version, which in fact is not the latest one. I think the latest one is in beta version, but that's ok for us!

Run the following commands on SSH Terminal:

  > $sudo npm install -g npm@3.0-latest
  
  > $npm -v
  
If the last command returns 3.1.3, then everything went well.


##**Phase 4| Installing Yeoman (YO)**

Run the following command on SSH Terminal: 

> $sudo npm install -g yo



##**Phase 5| Installing Meanjs**

Run the following command on SSH Terminal:

> $sudo npm install -g generator-meanjs


##**Phase 6| Installing Grunt**

Grunt is essential!
  
Run the following commands on SSH Terminal:

  > $sudo npm install -g grunt-init
  
  > $sudo npm install -g grunt-cli
  
  > $npm install grunt
